# Pronouns.page Logo

Brand logo for [Pronouns.page](https://pronouns.page/)

<img alt="Logo" src="logo.svg" width="200" />

## License

The logo is licensed under [OQL](LICENSE.md).
